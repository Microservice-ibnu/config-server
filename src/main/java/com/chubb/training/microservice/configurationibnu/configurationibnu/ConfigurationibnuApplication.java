package com.chubb.training.microservice.configurationibnu.configurationibnu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurationibnuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationibnuApplication.class, args);
	}
}
